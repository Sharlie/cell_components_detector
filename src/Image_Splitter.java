/*
 * Image Splitter
 * This class splits the images in smaller pieces.
 * 2017 (c) Sharlie van der Heide
 */

import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sharlie van der Heide
 */

public class Image_Splitter implements PlugInFilter {
    /**
     * Contains the image.
     */
    private ImagePlus ip;
    /**
     * Contains the title of the image.
     */
    String title;
    /**
     * Contains the width of the image.
     */
    int width;
    /**
     * Contains the height of the image.
     */
    int height;
    /**
     * Contains the directory of the image.
     */
    String directory;
    /**
     * Contains the directory of the image with the title.
     */
    String imageDirectory;
    /**
     * Contains the contents of the command line.
     */
    String cmd;
    /**
     * Contains the size of the image should be.
     */
    String imageSizepx;
    /**
     * Contains the title of the image without extension.
     */
    String titleWithoutExt;
    /**
     * Contains the directory to where the image should be copied.
     */
    String destination;
    /**
     * Contains the image size given in the dialog.
     */
    int imageSize = 0;
    /**
     * Contains the number of columns the image should be divided in.
     */
    int col;
    /**
     * Contains the number of rows the image should be divided in.
     */
    int row;
    /**
     * This method makes a diolog where the user can decide how big the splitted
     * images must be.
     * @return false if the dialog is cancelled
     */
    public boolean makeDialog() {
        String[] pixelChoices = new String[] {"2000", "1500", "1000"};
        GenericDialog gd = new GenericDialog("New Image Size");
        gd.addMessage("Enter a new image size: \n For using the Cell Components"
                + " Detector, images must be smaller than 2000px");
        gd.addChoice("Size image ~ <", pixelChoices, imageSizepx);
        gd.addMessage("Or chose an own image size:");
        gd.addNumericField("Size image ~ <", imageSize, 0);
        gd.showDialog();
        if (gd.wasCanceled()) {
            return false;
        } else {
            imageSizepx = gd.getNextChoice();
            imageSize = (int) gd.getNextNumber();
            return true;
        }
    }
    /**
     * This method sets the image size by taking the arguments from the dialog.
     * If the image size is 0, that means that the open field is not filled in.
     * So then image size must be the output of the choice field.
     */
    public void setImageSize() {
        if (imageSize == 0) {
            imageSize = Integer.parseInt(imageSizepx);
        }
    }
    /**
     * This method copies the image if the image name contains white spaces.
     * The image name of the copy has '_' in stead of ' '.
     * @param titleWithoutExt
     * @param directory
     * @param imageDirectory
     * @return the directory of the copied image
     * @throws IOException
     */
    public String copyRenameFile(String titleWithoutExt, String directory,
            String imageDirectory) throws IOException {
        titleWithoutExt = titleWithoutExt.replace(" ", "_");
        destination = directory + titleWithoutExt + ".png";
        Path FROM = Paths.get(imageDirectory);
        Path TO = Paths.get(destination);
        CopyOption[] options = new CopyOption[]{
          StandardCopyOption.REPLACE_EXISTING,
          StandardCopyOption.COPY_ATTRIBUTES
        };
        Files.copy(FROM, TO, options);
        return destination;
    }
    /**
     * This method calculates the number of columns and rows,
     * wherein the image will be divided.
     */
    public void calculateColRow() {
        //Making doubles because otherwise math.ceil doesnt work
        double widthDouble = (double) width;
        double heightDouble = (double) height;
        double imageSizeDouble = (double) imageSize;
        //Rounding up, so there wont be pixel leftovers after dividing
        col = (int) Math.ceil(widthDouble / imageSizeDouble);
        row = (int) Math.ceil(heightDouble / imageSizeDouble);
    }
    /**
     * This method makes a RGB image of the original image.
     * @param directory
     * @param title 
     */
    public void makeImageRGB(String directory, String title) {
        IJ.run(ip, "RGB Color", "");
        IJ.saveAs(ip, "Tiff", directory + title + "_RGB.tif");
        IJ.log("Changed image to RGB (copy)");
    }
    /**
     * This method splits the image with the help of the ImageMagick tool,
     * executed on the command line. But when the image name contains white
     * spaces, first the image will be copied.
     * @param imageDirectory the directory of the image with title
     * @param directory the directory of the image without the title
     * @param title the title of the image
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void splitImage(String imageDirectory, String directory,
            String title) throws IOException, InterruptedException {
        titleWithoutExt = title.substring(0, title.lastIndexOf("."));
        if (titleWithoutExt.contains(" ")) {
            imageDirectory = copyRenameFile(titleWithoutExt, directory,
                    imageDirectory);
            titleWithoutExt = titleWithoutExt.replace(" ", "_");
            IJ.log("The image is copied and renamed because "
                    + "the name contains white spaces");
            IJ.log("The image is renamed as: " + imageDirectory);
        }
        //If the whole image directory contains white spaces, the user gets an
        //error.
        if (imageDirectory.contains(" ")) {
            IJ.error("Your image directory contains white spaces, please fix "
                    + "this in order to use the plugin!");
        }
        //If the directory contains "\", then it is a windows directory
        if (imageDirectory.contains("\\")) {
            //Executing the ImageMagick tool on the command line with on
            // windows with "cmd /c convert"
            cmd = "cmd /c start/wait convert " + imageDirectory
               + " -crop " + col + "x" + row + "@ +repage +adjoin "
                    + directory + titleWithoutExt + "_" + col + "x"
                    + row + "@_%d.png";
        } else {
            //Executing the ImageMagick tool on the command line of linux
            cmd = "convert " + imageDirectory
               + " -crop " + col + "x" + row + "@ +repage +adjoin "
                    + directory + titleWithoutExt + "_" + col + "x"
                    + row + "@_%d.png";
        }
        IJ.log("Splitting image...");
        Process pr = Runtime.getRuntime().exec(cmd);
        pr.waitFor();
        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(pr.getInputStream()));
        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(pr.getErrorStream()));
        IJ.log("Done splitting image");
        IJ.log("Given size: ~ <" + imageSize + "\n"
                   + "Columns: " + col + "\n"
                   + "Rows: " + row + "\n"
                   + "The output images can be found in this directory: "
                   + directory + titleWithoutExt + "_" + col
                   + "x" + row + "@_%d.png");
    }
    /**
     * This method checks if there is an image.
     * @param arg the arguments that are given
     * @param ip a java class in ImageJ
     * @return DONE to stop the program if there is nog image or dialog is
     * cancelled
     */
    @Override
    public final int setup(String arg, ImagePlus ip) {
        this.ip = ip;
        if (this.ip == null) {
            IJ.error("No Image",
                    "There are no images open.");
            return DONE;
        } else if (this.makeDialog() == false) {
            return DONE;
        }
        return DOES_8G + DOES_16 + DOES_32;
    }
    /**
     * This method runs the other methods to split the image.
     * @param imp a java class in ImageJ
     */
    @Override
    public void run(ImageProcessor imp) {
        width = ip.getWidth();
        height = ip.getHeight();
        directory = ij.IJ.getDirectory("image");
        title = ip.getTitle();
        imageDirectory = directory + title;
        try {
            this.setImageSize();
            this.calculateColRow();
            try {
                this.splitImage(imageDirectory, directory, title);
                this.makeImageRGB(directory, titleWithoutExt);
            } catch (InterruptedException ex) {
                Logger.getLogger(Image_Splitter.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
            } catch (IOException ex) {
                IJ.error("The image could not be split");
                Logger.getLogger(Image_Splitter.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
    }
}