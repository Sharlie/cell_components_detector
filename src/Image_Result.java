/*
 * Image Result
 * This class manipulates the probability maps and then is stitching all the
 * classes together. After stitching, the classes are being deleted.
 * 2017 (c) Sharlie van der Heide
 */

import ij.IJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.plugin.Duplicator;
import ij.plugin.filter.PlugInFilter;
import static ij.plugin.filter.PlugInFilter.DOES_16;
import static ij.plugin.filter.PlugInFilter.DONE;
import ij.process.ImageProcessor;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sharlie van der Heide
 */

public class Image_Result implements PlugInFilter {
    /**
     * Contains the probability map.
     */
    private ImagePlus ip;
    /**
     * Contains the title of the probability map.
     */
    String title;
    /**
     * Contains the directory of the documents.
     */
    String documentsDirectory;
    /**
     * Contains the directory of the probability map.
     */
    String probMapDir;
    /**
     * Contains the size of the probability map.
     */
    int stackSize;
    /**
     * Contains a count for duplicating classes.
     */
    int count;
    /**
     * Contains a count for stitching classes.
     */
    int count2;
    int count3;
    /**
     * Contains a command line with the arguments.
     */
    String cmd;
    /**
     * Contains the title of the splitted images.
     */
    String outputTitle;
    /**
     * Contains the number of columns.
     */
    int colImage;
    /**
     * Contains the number of rows.
     */
    int rowImage;
    /** 
     * Contains the title of the probability map.
     */
    String probMapTitle;
    /**
     * Contains the title of the probability map without extension.
     */
    String probMapTitleWithoutExt;
    /**
     * This method gets and returns the size of the probability map.
     * @return size of the probability map
     */
    public int getStackSize() {
        stackSize = ip.getStackSize() + 1;
        return stackSize;
    }
    /**
     * This method gets the number of columns and rows the image was splitted
     * in.
     * @param outputName
     * @param col
     * @param row
     */
    public void getOutputNameColRow(String outputName, int col, int row) {
        outputTitle = outputName;
        colImage = col;
        rowImage = row;
    }
    /**
     * This method gets the title and directory of the probability map.
     * @param imageTitle
     * @param documentsDir 
     */
    public void getProbMapDirectory(String imageTitle, String documentsDir) {
        title = imageTitle.substring(0, imageTitle.lastIndexOf("."));
        documentsDirectory = documentsDir;
        probMapTitle = title + "_ProbabilityMap.tif";
        probMapTitleWithoutExt = title + "_ProbabilityMap";
        probMapDir = documentsDirectory + title + "_ProbabilityMap.tif";
    }
    /**
     * This method duplicates the classes and sets a threshold.
     */
    public void duplicateClasses() {
        stackSize = ip.getStackSize() + 1;
        count = 1;
        IJ.run(ip, "16-bit", "");
        IJ.run(ip, "Save", "");
        //For each class in the probability map, the class is duplicated and
        //set to a threshold.
        while (count < stackSize) {
            ImagePlus imgClass = new Duplicator().run(ip, count, count);
//            IJ.run(imgClass, "Color Balance...", "");
//            IJ.run(imgClass, "Enhance Contrast", "saturated=0.35");
//            IJ.run(imgClass, "Apply LUT", "");
//            IJ.run("Close");
//            IJ.setRawThreshold(imgClass, 50000, 65535, null);
////            Prefs.blackBackground = false;
//            IJ.run(imgClass, "Convert to Mask", "");
            IJ.saveAs(imgClass, "Tiff", documentsDirectory + title + "_Class"
                    + count + ".tif");
            imgClass.close();
            count++;
        }
    }
    /**
     * This method is stitching the classes to each other to make an overlay for
     * the original image.
     * @param size
     * @param titleWithoutExt
     * @param numberImages
     * @throws IOException
     * @throws InterruptedException
     */
    public void stitchingClasses(int size, String titleWithoutExt,
            int numberImages) throws IOException, InterruptedException {
        title = titleWithoutExt;
        stackSize = size;
        count2 = 1;
        int numImages = numberImages;
        //For each class, the splitted images are added to a list.
        while (count2 < stackSize) {
            List<String> classList = new ArrayList<>();
//            File folderResults = new File(documentsDirectory);
//            File[] fileListResult = folderResults.listFiles();
            count3 = 0;
            while (count3 < numImages) {
                String className = outputTitle + count3 + "_Class" + count2
                        + ".tif";
                classList.add(documentsDirectory + className);
//                for (int i = 0; i < fileListResult.length; i++) {
//                    String fileName = fileListResult[i].getName();
//                    if (fileName.equals(className)) {
//                        classList.add(documentsDirectory + className);
//                    }
//                }
                count3++;
            }
            //Then the list is made to a String, so it can be used to stitch
            //the classes to each other.
            String classes = classList.toString();
            classes = classes.substring(1, classes.length() - 1);
            classes = classes.replaceAll(",", "");
            IJ.log("classes: " + classes);
            //If the directory contains \, it means it is a Windows PC,
            //so first, cmd must be executed.
            if (documentsDirectory.contains("\\")) {
               cmd = "cmd /c start/wait montage " + classes
                       + " -geometry +0+0 -tile " + colImage + "x" + rowImage
                        + " " + documentsDirectory + title + "_Class" + count2
                       + "_Stitched.tif";
            } else {
                cmd = "start/wait montage " + classes + " -geometry +0+0 -tile "
                        + colImage + "x" + rowImage + " " + documentsDirectory
                        + title + "_Class" + count2 + "_Stitched.tif";
            }
            Process pr = Runtime.getRuntime().exec(cmd);
            pr.waitFor();
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(pr.getInputStream()));
            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(pr.getErrorStream()));
            IJ.log("CMD: " + cmd);
            count2++;
        }
    }
        /**
         * This method deletes the classes after being stitched together.
         * @param stackSize
         * @param documentsDirectory
         * @param outputName
         * @throws IOException
         */
        public void deleteClasses(int stackSize, String documentsDirectory,
                String outputName) throws IOException {
            File folderResults = new File(documentsDirectory);
            File[] fileListResult = folderResults.listFiles();
            for (int i = 0; i < fileListResult.length; i++) {
                String fileName = fileListResult[i].getName();
                //If the name in the folder, start with the output name, it
                //means it can be deleted.
                if (fileName.startsWith(outputName)) {
                    Path p = Paths.get(fileListResult[i].toString());
                    Files.delete(p);
                }
            }
        }
    /**
     * This method opens the probabilty map and otherwise gives an error.
     * @param arg
     * @param imp
     * @return DONE if the image is not found
     */
    @Override
    public final int setup(String arg, ImagePlus imp) {
        File f = new File(probMapDir);
        if (f.exists() && !f.isDirectory()) {
            ip = IJ.openImage(probMapDir);
        } else {
            IJ.error("No image found in " + probMapDir);
            return DONE;
        }
        return DOES_STACKS + DOES_16;
    }
    /**
     * This method executes the duplicate classes method.
     * @param imp
     */
    @Override
    public void run(ImageProcessor imp) {
        this.duplicateClasses();
    }
}