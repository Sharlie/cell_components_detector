/*
 * Image Classifier
 * This class can classify the image(s) with the Trainable Weka Segmentation
 * Plugin from FIJI.
 * 2017 (c) Sharlie van der Heide
 */

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import static ij.plugin.filter.PlugInFilter.DOES_16;
import static ij.plugin.filter.PlugInFilter.DOES_32;
import static ij.plugin.filter.PlugInFilter.DOES_8G;
import ij.process.ImageProcessor;
import java.io.File;
import trainableSegmentation.WekaSegmentation;

/**
 *
 * @author Sharlie van der Heide
 */

public class Image_Classifier implements PlugInFilter {
    /**
     * Contains the image.
     */
    private ImagePlus ip;
    /**
     * Contains the directory with the title.
     */
    String imgDirectory;
    /**
     * Contains the directory of the image.
     */
    String directory;
    /**
     * Contains the directory where the classifier is in.
     */
    String documentsDirectory;
    /**
     * Contains the title of the image.
     */
    String title;
    /**
     * This method gets the directories and the title of the image.
     * @param imageDirectory
     * @param dir
     * @param imageTitle
     * @param documentsDir
     */
    public void getImageDirectory(String imageDirectory,
            String dir, String imageTitle, String documentsDir) {
        imgDirectory = imageDirectory;
        directory = dir;
        title = imageTitle.substring(0, imageTitle.lastIndexOf("."));
        documentsDirectory = documentsDir;
    }
    /**
     * This method makes the original image a RGB image.
     * @param title
     * @param directory
     */
    public void makeImageRGB(String title, String directory) {
        IJ.run(ip, "RGB Color", "");
        IJ.saveAs(ip, "Tiff", directory + title + "_RGB.tif");
        IJ.log("Changed image to RGB (copy)");
    }
    /**
     * This method classifies the image with the use of the Trainable Weka
     * Segmentation plugin. After the classification, the probability maps
     * where saved in the same map as the classifier.
     */
    public void classifyImage() {
        WekaSegmentation segmentator = new WekaSegmentation(ip);
        File model = new File(this.documentsDirectory
               + "Cell_Components_Detector.model");
        File arff = new File(this.documentsDirectory
                + "Cell_Components_Detector.arff");
        if (model.exists() && arff.exists()) {
            IJ.log("Loading classifier...");
            segmentator.loadClassifier(this.documentsDirectory
                   + "Cell_Components_Detector.model");
            IJ.log("Loaded Cell_Components_Detector.model");
            IJ.log("Loading Cell_Components_Detector.arff");
            segmentator.loadTrainingData(this.documentsDirectory
                + "Cell_Components_Detector.arff");
            IJ.log("Loaded Cell_Components_Detector.arff");
            IJ.log("Applying classifier...");
            ImagePlus probabilityMap = segmentator.applyClassifier(ip, 0, true);
            IJ.save(probabilityMap, documentsDirectory + title
                   + "_ProbabilityMap.tif");
            probabilityMap.close();
            ip.close();
            IJ.log("Saved probability map: " + documentsDirectory
                   + title + "_ProbabilityMap.tif");
        } else {
            IJ.log("Classifing is stopped: no classifier or arff found at "
                   + documentsDirectory);
            IJ.error("Can not classify, the classifier or arff is not "
                    + "in this directory : " + documentsDirectory);
        }
    }
    /**
     * This method gets the directory of the classifier and checks if the
     * image exists.
     * @param arg
     * @param imp
     * @return DONE if the image is not found
     */
    @Override
    public final int setup(String arg, ImagePlus imp) {
        File f = new File(imgDirectory);
        if (f.exists() && !f.isDirectory()) {
            ip = IJ.openImage(imgDirectory);
        } else {
            IJ.error("No image found in " + imgDirectory);
            return DONE;
        }
        return DOES_8G + DOES_16 + DOES_32;
    }
    /**
     * This method executes the classify method.
     * @param imp
     */
    @Override
    public void run(ImageProcessor imp) {
        this.classifyImage();
    }
}
