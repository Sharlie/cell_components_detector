/*
 * Image Overlays
 * This class can make a Photoshop file with the original image and his
 * overlays.
 * 2017 (c) Sharlie van der Heide
 */

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.Analyzer;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sharlie van der Heide
 */

public class Image_Overlays implements PlugInFilter {
    /**
    * Contains the image.
    */
    ImagePlus ip;
    /**
     * Contains the size of the probability map.
     */
    int stackSize;
    /**
     * Contains the directory of the classes.
     */
    String classDirectory;
    /**
     * Contains the number of the class.
     */
    int classInt;
    /**
     * Contains the directory of the documents.
     */
    String documentsDirectory;
    /**
     * Contains the directory to the original image as RGB.
     */
    String originalImage;
    /**
     * Contains the title of the original image.
     */
    String imageTitle;
    /**
     * Contains the command line arguments.
     */
    String cmd;
    /**
     * Contains a list of the classes used for the overlays.
     */
    String classList;
    /**
     * Contains the title without extension.
     */
    String titleWithoutExt;
    /**
     * This method gets the directories of the classes.
     * @param title
     * @param count
     * @param documentsDir
     */
    public void getClassDir(String title, int count, String documentsDir) {
        classDirectory = documentsDir + title;
        classInt = count - 1;
        documentsDirectory = documentsDir;
        titleWithoutExt = title.substring(0, title.lastIndexOf("."));
    }
    /**
     * This method gets the stack size of the probability map.
     * @param sizeStack
     */
    public void getStackSize(int sizeStack) {
        stackSize = sizeStack;
    }
    /**
     * This method adds color to the classes.
     * @throws java.io.IOException
     */
    public void addLUT() throws IOException {
        String[] listLUT = {"Red", "Green", "Blue", "Magenta", "Yellow", "Cyan",
            "Red", "Fire"};
        IJ.run(ip, "Color Balance...", "");
        IJ.run(ip, "Enhance Contrast", "saturated=0.35");
        IJ.run(ip, "Apply LUT", "");
        IJ.run("Close");
        IJ.setRawThreshold(ip, 37000, 65535, null);
//            Prefs.blackBackground = false;
        IJ.run(ip, "Convert to Mask", "");
//        IJ.run(ip, "Invert", "");
        IJ.run(ip, listLUT[classInt], "");
        IJ.run(ip, "RGB Color", "");
        IJ.saveAs(ip, "Tiff", documentsDirectory + titleWithoutExt + ".tif");
        IJ.log("Saved again in (with lut): " + documentsDirectory
                + titleWithoutExt + ".tif");
        Analyzer an = new Analyzer(ip);
        an.measure();
        float[] uMeans = an.getUMeans();
        //If the means of the image is bigger than 0, it means that the class
        //is not empty.
        if (uMeans[0] > 0) {
            Cell_Components_Detector.CLASSES.add(documentsDirectory
                    + titleWithoutExt + ".tif");
        }
    }
    /**
     * This method makes a Photoshop file from the RGB original image and the
     * colored overlays (classes).
     * @param title
     * @param imageDirectory
     * @param RGBDirectory
     * @throws IOException
     * @throws InterruptedException
     */
    public void makePhotoshop(String title, String imageDirectory,
            String RGBDirectory) throws IOException, InterruptedException {
        imageTitle = title;
        originalImage = RGBDirectory + imageTitle + "_RGB.tif";
        //Here the classes list is made to a String, so this string can be used
        //for make the Photoshop format.
        classList = Cell_Components_Detector.CLASSES.toString();
        classList = classList.substring(1, classList.length() - 1);
        classList = classList.replaceAll(",", "");
        //If the directory contains \, it means it is a windows PC, so first
        //cmd must be executed.
        if (documentsDirectory.contains("\\")) {
               cmd = "cmd /c start/wait convert " + classList
                       + " -transparent black " + originalImage
                       + " -clone 1 -reverse " + documentsDirectory + imageTitle
                       + "_Result.psb";
            } else {
                cmd = "start/wait convert " + classList + " -transparent black "
                        + originalImage + " -clone 1 -reverse "
                        + documentsDirectory + imageTitle + "_Result.psb";
            }
        IJ.log("Making Photoshop file...");
            Process pr = Runtime.getRuntime().exec(cmd);
            pr.waitFor();
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(pr.getInputStream()));
            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(pr.getErrorStream()));
        IJ.log("Done making Photoshop file! \n"
                + "Can be found in: " + documentsDirectory + imageTitle
                + "_Result.psb");
    }
    /**
     * This method opens the image or gives an error if not found.
     * @param arg
     * @param imp
     * @return
     */
    @Override
    public final int setup(String arg, ImagePlus imp) {
        File f = new File(classDirectory);
        if (f.exists() && !f.isDirectory()) { //of !f.isDirectory()
            ip = IJ.openImage(classDirectory);
        } else {
            IJ.error("No image found in " + classDirectory);
            return DONE;
        }
        return DOES_8G + DOES_16 + DOES_32;
    }
    /**
     * This method executes the add LUT method.
     * @param imp
     */
    @Override
    public void run(ImageProcessor imp) {
        try {
            this.addLUT();
        } catch (IOException ex) {
            Logger.getLogger(Image_Overlays.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }
}
