# Cell Components Detector #

Program for detecting cell components in Electron Microscope images.

### INTRODUCTION ###

* Detecting mitochondria, zymogen, alpha-vesicles, beta-vesicles, endoplasmic reticulum,
 and background in large Electron Microscope images of the pancreas
* Version 0.01
* 2016(c) copyright Sharlie van der Heide, Hanze University of Applied Sciences Groningen (NL)
  Internship at the cell biology department at the University Medical Center Groningen

### REQUIREMENTS ###

* FIJI 
	- At least 2.0.0
	- Download the most recent version of Fiji from [FIJI](https://imagej.net/Fiji/Downloads)
* JAVA 
	- Make sure that FIJI runs on at least JAVA 1.8
	- Download the most recent version of JAVA from [JAVA](https://www.java.com/nl/download/)
* Trainable Weka Segmentation plugin
	- Make sure that the Trainable Weka Segmentation is installed in FIJI
* ImageMagick
	- Make sure that ImageMagick is properly installed on your device
	- Download the most recent version of ImageMagick from [ImageMagick](https://sourceforge.net/projects/imagemagick/files/im7-exes/)
	- In the program this version is used: ImageMagick-7.0.3-6-Q16-x64-dll.exe
	** During the install of ImageMagick make sure the next points are checked: ¨Add application directory to your system path¨ and ¨Install legacy utilities (e.g. convert)¨
	    Otherwise the program will not run!
* Photoshop
	- Make sure that you have Photoshop installed. The result of the plugin is a Photoshop-format (.psd).

### HOW DO I SET UP ###

* You first need to download the Cell_Components_Detector.jar from the Downloads-section and install this in FIJI: Plugins > Install PlugIn
* Then you need to download the Cell_Components_Detector.model and Cell_Components_Detector.arff as classifier for the images:
	- This model and arff needs to be downloaded in your documents directory in a map named ¨Cell_Components_Detector¨
	* This directory is also the directory for the output of the plugin
* Then you can select the plugin in FIJI at: Plugins > Cell Components Detector

### USAGE ###

* Cell Components Detector
	- This plugin splits the image if the image is bigger than 2000px, then the images will be classified with the model
	  After the classification, there is made a Photoshop format with all the different classes as overlays over the original image

* Split Images
	- This plugin is only a splitter, to split images in the size you want
	** This plugin is used in the Cell Components Detector if the image is bigger than 2000 px

### Information or questions ###

For more information you can check the ¨How to¨ document in the Downloads-section.
For questions you can contact me:
Sharlie van der Heide -> sharlie.v.d.heide@hotmail.com
