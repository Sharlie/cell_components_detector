/*
 * Cell Components Detector
 * This project can split images in smaller images and can detect
 * cell components by the use of the Trainable Weka Segmentation plugin.
 * This program can also make a Photoshop image with
 * the colored cell components as overlay.
 * 2017 (c) Sharlie van der Heide
 */

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Sharlie van der Heide
 */

public class Cell_Components_Detector implements PlugInFilter {
    /**
     * Contains a list of classes.
     */
    public static final List<String> CLASSES = new ArrayList<String>();
    /**
     * Contains the image.
     */
    private ImagePlus ip;
    /**
     * Contains the width of the image.
     */
    int width;
    /**
     * Contains the height of the image.
     */
    int height;
    /**
     * Contains the name of the image without the extension.
     */
    String titleWithoutExt;
    /**
     * Contains the arguments.
     */
    String args;
    /**
     * Contains the directory of the image.
     */
    String directory;
    /**
     * Contains the output name after splitting.
     */
    String outputName;
    /**
     * Contains the title of the image with extension.
     */
    String title;
    /**
     * Contains the directory with the title.
     */
    String imageDirectory;
    /**
     * Contains the number of images after splitting.
     */
    int numberImages;
    /**
     * Contains the size of the probability map.
     */
    int stackSize;
    /**
     * Contains the name of the class.
     */
    String className;
    /**
     * This method is used to generate an "about" string of the plugin
     * @return about string
     */
    String documentsDirectory;
    public final String showAbout() {
        String about = "Cell Components Detector, plugin for detecting cell "
                + " components \n(mitochondria, zymogen, alpha-vesicles, "
                + "beta-vesicles, erythrocyte, endoplasmic reticulum, "
                + "nuclei and background) in large \n Electron "
                + "Microscope images of the pancreas. \n"
                + "Created by Sharlie van der Heide. \n"
                + "Hanze University of Applied Sciences of Groningen. \n"
                + "Internship at the cell biology department at the University "
                + "Medical Center Groningen. (NL) \n \n"
                + "-Cell Components Detector- \n This plugin splits the image "
                + "if the image is bigger than 2000px, \n then the images will "
                + "be classified with the model and arff \n"
                + "After the classification, there is made a Photoshop format "
                + "\n with all the different classes as overlays over the "
                + "original image. \n \n"
                + "-Split Images- \n This plugin is only a splitter, to split "
                + "images in the size you want. \n This plugin is also used in "
                + "the Cell Components Detector if the image is bigger than"
                + "2000px. \n \n"
                + "The output of the plugin can be found in your Documents "
                + "directory in a folder named Cell_Components_Detector (if "
                + "already created). \n \n"
                + "For more information and the needed classifier and arff for "
                + "running the classification can be found at: \n"
                + "Bitbucket: https://bitbucket.org/Sharlie/cell_components_"
                + "detector/overview";
        IJ.showMessage(about);
        return about;
    }
    /**
     * This method checks if the user clicked on the Help>About Plugins>Cell
     * Components Detector..., then the "about" string is printed. It also
     * checks if there is an image open.
     * @param arg the arguments that are given
     * @param ip the image
     * @return DONE stops the program, return DOES executes the run method
     */
    @Override
    public final int setup(String arg, ImagePlus ip) {
        args = arg;
        this.ip = ip;
        if (args.equals("about")) {
            this.showAbout();
            return PlugInFilter.DONE;
        } else if (this.ip == null) {
            IJ.error("No Image",
                    "There are no images open.");
            return DONE;
        }
        //Only images with gray-levels with 8/16/32 bits are admitted.
        return DOES_8G + DOES_16 + DOES_32;
    }
    /**
     * This method runs the Image_Splitter if necessary and
     * then the Image_Classifier.
     * When the image is bigger than 2000 px,
     * the image first needs to be splitted.
     * @param imp image
     */
    @Override
    public void run(ImageProcessor imp) {
        //Start time of the program.
        DateFormat df = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        Date date1 = new Date();
        String startTime = df.format(date1);
        IJ.log("Start time: " + startTime);
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        ip.unlock();
        directory = ij.IJ.getDirectory("image");
        title = ip.getTitle();
        titleWithoutExt = title.substring(0, title.lastIndexOf("."));
        imageDirectory = directory + title;
        //Saves as PNG, otherwise the splitter does not work.
        IJ.saveAs(ip, "PNG", directory + titleWithoutExt + ".png");
        IJ.log("Changed image to PNG (copy)");
        ip = IJ.openImage(directory + titleWithoutExt + ".png");
        title = ip.getTitle();
        titleWithoutExt = title.substring(0, title.lastIndexOf("."));
        imageDirectory = directory + title;
        width = ip.getWidth();
        height = ip.getHeight();
        Image_Splitter ims = new Image_Splitter();
        //Sets the documents directory to the Cell_Components_Detector folder
        //in the Documents map of the user.
        documentsDirectory = System.getProperty("user.home") + File.separator
               + "Documents" + File.separator + "Cell_Components_Detector"
               + File.separator;
        Image_Classifier imc = new Image_Classifier();
        Image_Result imr = new Image_Result();
        Image_Overlays imo = new Image_Overlays();
        //If the image is bigger than 2000x2000px, the image first need to be
        //splitted.
        if (width > 2000 || height > 2000) {
            int count = 0;
            int count3 = 1;
            //If setup did not returned DONE, the image can be processed.
            if (ims.setup(args, ip) == DOES_8G + DOES_16 + DOES_32) {
                ims.run(imp);
                directory = ims.directory;
                numberImages = ims.col * ims.row;
                titleWithoutExt = ims.titleWithoutExt;
                title = titleWithoutExt + ".png";
                outputName = titleWithoutExt + "_" + Integer.toString(ims.col)
                        + "x" + Integer.toString(ims.row) + "@_";
                int imagesNotProcessed = numberImages;
                //For each number in numberimages, classify the image and
                //process the image in image result.
                while (count < numberImages) {
                    //Time of starting a splitted image.
                    Date classStartTime = new Date();
                    title = outputName + Integer.toString(count) + ".png";
                    imageDirectory = directory + title;
                    //Running the Image_Classifier class.
                    imc.getImageDirectory(imageDirectory, directory, title
                            , documentsDirectory);
                    imc.setup(args, ip);
                    imc.run(imp);
                    //Running the Image_Result class.
                    imr.getOutputNameColRow(outputName, ims.col, ims.row);
                    imr.getProbMapDirectory(title, documentsDirectory);
                    imr.setup(args, ip);
                    imr.run(imp);
                    IJ.log("Done processing this image: " + title);
                    Path p = Paths.get(directory + title);
                    String titleWithoutExtension = title.substring(0
                            , title.lastIndexOf("."));
                    Path pProb = Paths.get(documentsDirectory
                            + titleWithoutExtension + "_ProbabilityMap.tif");
//                    try {
                        //Deleting the image after processing.
//                        Files.delete(p);
//                        IJ.log("Deleted this image: " + title);
                        //Deleting the probability map after processing.
//                        Files.delete(pProb);
//                        IJ.log("Deleted this probability map: "
//                                + titleWithoutExtension
//                                + "_ProbabilityMap.tif");
//                    } catch (IOException ex) {
//                        Logger.getLogger(
//                                Cell_Components_Detector.class.getName())
//                                .log(Level.SEVERE, null, ex);
//                    }
                    count++;
                    imagesNotProcessed--;
                    IJ.log("Images not processed yet: " + imagesNotProcessed);
                    //Time after the splitted image is processed.
                    Date classTime = new Date();
                    String time = df.format(classTime);
                    //Substracts the time after processing with the start time.
                    long diff = Math.abs(classTime.getTime()
                            - classStartTime.getTime());
                    //Then this time will be multiplied with the number
                    //of images that are not processed yet. This will be the
                    //remaining time.
                    long processingTime = diff * imagesNotProcessed;
                    long diffDays = processingTime / daysInMilli;
                    processingTime = processingTime % daysInMilli;
                    long diffHours = processingTime / hoursInMilli;
                    processingTime = processingTime % hoursInMilli;
                    long diffMin = processingTime / minutesInMilli;
                    processingTime = processingTime % minutesInMilli;
                    long diffSec = processingTime / secondsInMilli;
                    IJ.log("Time: " + time);
                    IJ.log("Remaining time: " + diffDays + " days, " + diffHours
                            + " hours, " + diffMin + " minutes, " + diffSec
                            + " seconds");
                }
                try {
                    stackSize = imr.stackSize;
                    //Here the classes of the splitted images are stitched
                    //together.
                    imr.stitchingClasses(stackSize, titleWithoutExt,
                            numberImages);
                    //Then these classes are deleted.
//                    imr.deleteClasses(stackSize, documentsDirectory,
//                            outputName);
                } catch (IOException | InterruptedException ex) {
                    Logger.getLogger(Cell_Components_Detector.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
                while (count3 < stackSize) {
                    className = titleWithoutExt + "_Class" + count3
                            + "_Stitched.tif";
                    //For each class, the stitched image, the Image_Overlays
                    //class is executed.
                    imo.getClassDir(className, count3, documentsDirectory);
                    imo.getStackSize(stackSize);
                    imo.setup(args, ip);
                    imo.run(imp);
                    count3++;
                }
                try {
                    ip.close();
                    //The Photoshop image will be made.
                    imo.makePhotoshop(titleWithoutExt, ims.imageDirectory,
                            directory);
                } catch (IOException | InterruptedException ex) {
                    Logger.getLogger(Cell_Components_Detector.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
            } else {
                //If setup returned DONE, an error as given to the user.
                IJ.error("You stopped");
            }
        //Otherwise, if the image is smaller than 2000, the image can be
        // classified.
        } else {
            int count4 = 1;
            directory = ij.IJ.getDirectory("image");
            imageDirectory = directory + title;
            //If the title contains white spaces, first the image must be
            //renamed.
            if (titleWithoutExt.contains(" ")) {
                try {
                    //Renaming the image.
                    imageDirectory = ims.copyRenameFile(titleWithoutExt,
                            directory, imageDirectory);
                    titleWithoutExt = titleWithoutExt.replace(" ", "_");
                    title = titleWithoutExt + ".tif";
                } catch (IOException ex) {
                    Logger.getLogger(Cell_Components_Detector.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
                IJ.log("The image is copied and renamed because "
                        + "the name contains white spaces");
                IJ.log("The image is renamed as: " + imageDirectory);
            }
            //Executing the Image_Classifier class.
            imc.makeImageRGB(titleWithoutExt, directory);
            imc.getImageDirectory(imageDirectory, directory, title,
                    documentsDirectory);
            imc.setup(args, ip);
            imc.run(imp);
            //Executing the Image_Result class.
            imr.getProbMapDirectory(title, documentsDirectory);
            imr.setup(args, ip);
            imr.run(imp);
            titleWithoutExt = title.substring(0, title.lastIndexOf("."));
            String probMap = documentsDirectory + titleWithoutExt
                    + "_ProbabilityMap.tif";
            Path p = Paths.get(probMap);
            try {
                //Deleting the probability map after the image is processed.
                Files.delete(p);
                IJ.log("Deleted this probability map: " + probMap);
            } catch (IOException ex) {
                Logger.getLogger(Cell_Components_Detector.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
            stackSize = imr.stackSize;
            while (count4 < stackSize) {
                //For each class the Image_Overlays class is executed.
                className = titleWithoutExt + "_Class" + count4 + ".tif";
                imo.getClassDir(className, count4, documentsDirectory);
                imo.getStackSize(stackSize);
                imo.setup(args, ip);
                imo.run(imp);
                count4++;
            }
            try {
                ip.close();
                //Making the Photoshop image.
                imo.makePhotoshop(titleWithoutExt, imageDirectory, directory);
            } catch (IOException | InterruptedException ex) {
                Logger.getLogger(Cell_Components_Detector.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
        //Making the classes list empty.
        CLASSES.clear();
        //Getting the time when te program is done.
        Date date2 = new Date();
        String endTime = df.format(date2);
        IJ.log("End time: " + endTime);
        //Substracting the end time with the start time to get the elapsed time.
        long diff = Math.abs(date2.getTime() - date1.getTime());
        long diffDays = diff / daysInMilli;
        diff = diff % daysInMilli;
        long diffHours = diff / hoursInMilli;
        diff = diff % hoursInMilli;
        long diffMin = diff / minutesInMilli;
        diff = diff % minutesInMilli;
        long diffSec = diff / secondsInMilli;
        IJ.log("Elapsed time: " + diffDays + " days, " + diffHours + " hours, "
                + diffMin + " minutes, " + diffSec + " seconds");
    }
}